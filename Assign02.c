//This is a program to identify if a number is odd or even
#include <stdio.h>
int main (){
    int number;

    //Entering a number by the user
    printf ("Please enter a number: ");

    //Reading the entered number
    scanf ("%d", &number);

    //Checking if the entered number is odd or evem
    if (number%2==0)
	    printf ("%d is an EVEN number", number);
    else 
	    printf ("%d is an ODD number", number);

    printf ("\n");

    return 0;
}

