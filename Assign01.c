//Program to find if a number is positive, negative or zero
#include <stdio.h>
int main (){
    int number;

    //Entering a number by the user
    printf ("Please enter a number: ");

    //Reading the entered number
    scanf ("%d", &number);

    //Checking if the entered number is positive, negative or zero
    if (number>0)
	    printf ("The number is POSITIVE", number);
    else if (number<0)
	    printf ("The number is NEGATIVE", number);
    else if (number==0)
	    printf ("The number is ZERO", number);

    printf ("\n");


    return 0;
}
